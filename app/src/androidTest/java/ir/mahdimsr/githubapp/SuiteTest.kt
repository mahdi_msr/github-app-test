package ir.mahdimsr.githubapp

import ir.mahdimsr.githubapp.ui.LoginActivityTest
import ir.mahdimsr.githubapp.ui.MainActivityTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.runner.RunWith
import org.junit.runners.Suite

/*
* this class define ro RUN all androidTest
*
* */


@ExperimentalCoroutinesApi
@RunWith(Suite::class)
@Suite.SuiteClasses(
        LoginActivityTest::class,
        MainActivityTest::class,
        DatabaseInstrumentedTest::class
                   )
class SuiteTest

