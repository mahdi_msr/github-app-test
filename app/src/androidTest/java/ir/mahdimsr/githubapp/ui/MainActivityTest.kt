package ir.mahdimsr.githubapp.ui

import android.content.Context
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import ir.mahdimsr.githubapp.R
import ir.mahdimsr.githubapp.retrofit.TokenManager
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class MainActivityTest : TestCase()
{
    @get : Rule
    val mainActivityScenario = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun test_isActivityInView()
    {

        val context = ApplicationProvider.getApplicationContext<Context>()

        val tokenManager = TokenManager(context)

        if (tokenManager.hasToken())
        {
            onView(withId(R.id.fragment)).check(matches(isDisplayed()))

            onView(withId(R.id.bottomNavigationView)).check(matches(isDisplayed()))
        }
        else
        {
            onView(withId(R.id.fragment)).check(doesNotExist())

            onView(withId(R.id.bottomNavigationView)).check(doesNotExist())
        }
    }

    @Test
    fun test_bottomMenu_click_followerFragment()
    {
        onView(withId(R.id.followerFragment)).perform(click())

        onView(withId(R.id.followerFragmentParent)).check(matches(isDisplayed()))
    }

    @Test
    fun test_bottomMenu_click_starredFragment()
    {
        onView(withId(R.id.starredFragment)).perform(click())

        onView(withId(R.id.starredFragmentParent)).check(matches(isDisplayed()))
    }

    @Test
    fun test_bottomMenu_click_subscribeFragment()
    {
        onView(withId(R.id.subscribeFragment)).perform(click())

        onView(withId(R.id.subscribeFragmentParent)).check(matches(isDisplayed()))
    }

    @Test
    fun test_onBackPress_navigate_LoginActivity()
    {
        pressBack()

        onView(withId(R.id.loginParentLayout)).check(matches(isDisplayed()))
    }
}