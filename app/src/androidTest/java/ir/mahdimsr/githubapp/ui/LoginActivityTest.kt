package ir.mahdimsr.githubapp.ui

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import ir.mahdimsr.githubapp.R
import ir.mahdimsr.githubapp.retrofit.TokenManager
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class LoginActivityTest : TestCase()
{
    @get : Rule
    val loginActivityScenario = ActivityScenarioRule(LoginActivity::class.java)

    private lateinit var tokenManager: TokenManager


    @Before
    public override fun setUp()
    {
        val context = ApplicationProvider.getApplicationContext<Context>()

        tokenManager = TokenManager(context)
    }

    @Test
    fun test_isActivityInView()
    {


        onView(withId(R.id.loginParentLayout)).check(matches(isDisplayed()))

    }

    @Test
    fun test_visibility_EditText_Button()
    {


        // editText
        onView(withId(R.id.editText)).check(matches(isDisplayed()))

        // button
        onView(withId(R.id.loginButton)).check(matches(isDisplayed()))
    }


    @Test
    fun test_navigateTo_MainActivity()
    {
        onView(withId(R.id.editText)).perform(typeText("mahdimsr"))

        onView(withId(R.id.loginButton)).perform(click())

        onView(withId(R.id.fragment)).check(matches(isDisplayed()))
    }

    @Test
    fun test_not_navigateTo_MainActivity()
    {
        onView(withId(R.id.loginButton)).perform(click())

        onView(withId(R.id.fragment)).check(doesNotExist())
    }


}