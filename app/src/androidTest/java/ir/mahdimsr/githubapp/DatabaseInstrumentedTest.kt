package ir.mahdimsr.githubapp

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import ir.mahdimsr.githubapp.room.AppDB
import ir.mahdimsr.githubapp.room.dao.FollowerDao
import ir.mahdimsr.githubapp.room.dao.StarredDao
import ir.mahdimsr.githubapp.room.dao.SubscribeDao
import ir.mahdimsr.githubapp.room.entity.FollowerRoomEntity
import ir.mahdimsr.githubapp.room.entity.StarredRoomEntity
import ir.mahdimsr.githubapp.room.entity.SubscribeRoomEntity
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DatabaseInstrumentedTest : TestCase()
{
    private lateinit var db: AppDB
    private lateinit var followerDao: FollowerDao
    private lateinit var starredDao: StarredDao
    private lateinit var subscribeDao: SubscribeDao


    @Before
    public override fun setUp()
    {
        val context = ApplicationProvider.getApplicationContext<Context>()

        db = Room.inMemoryDatabaseBuilder(context, AppDB::class.java).build()

        followerDao = db.followerDao
        starredDao = db.starredDao
        subscribeDao = db.subscribeDao
    }

    @After
    fun finish()
    {
        db.close()
    }

    @Test
    fun testFollowerCRUD() = runBlocking {

        // test insert
        var follower = FollowerRoomEntity(1, "mike", "nothing")

        followerDao.insert(follower)

        assertThat(follower == followerDao.select(1)).isTrue()

        // test update
        follower.name = "connor"

        followerDao.update(follower)

        assertThat(follower.name == followerDao.select(1)?.name).isTrue()

        // test delete
        followerDao.delete(follower)

        assertThat(followerDao.select(1) == null).isTrue()
    }

    @Test
    fun testStarredCRUD() = runBlocking {

        // test insert
        var starred = StarredRoomEntity(1,"mike" , "Box Technique")

        starredDao.insert(starred)

        assertThat(starredDao.select(1) == starred).isTrue()

        // test update
        starred.name = "conner";

        starredDao.update(starred)

        assertThat(starred.name == starredDao.select(1)?.name).isTrue()


        // test delete
        starredDao.delete(starred)

        assertThat(starredDao.select(1) == null).isTrue()
    }

    @Test
    fun testSubscribeCrud() = runBlocking {

        // test insert
        var subscribe = SubscribeRoomEntity(1,"mike","Box Technique")

        subscribeDao.insert(subscribe)

        assertThat(subscribeDao.select(1) == subscribe).isTrue()

        // test update
        subscribe.name = "conner"

        subscribeDao.update(subscribe)

        assertThat(subscribeDao.select(1).name == subscribe.name).isTrue()

        // test delete
        subscribeDao.delete(subscribe)

        assertThat(subscribeDao.select(1) == null).isTrue()


    }
}