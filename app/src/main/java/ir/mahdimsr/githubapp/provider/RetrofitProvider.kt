package ir.mahdimsr.githubapp.provider

import android.content.Context
import android.util.Log
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityRetainedScoped
import dagger.hilt.components.SingletonComponent
import ir.mahdimsr.githubapp.retrofit.Api
import ir.mahdimsr.githubapp.retrofit.TokenManager
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ActivityRetainedComponent::class)
object RetrofitProvider
{

    @ActivityRetainedScoped
    @Provides
    fun provideRetrofit(tokenManager: TokenManager): Retrofit
    {
        val username = tokenManager.getToken()

        val baseUrl = "https://api.github.com/users/$username/"


        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

    }

    @ActivityRetainedScoped
    @Provides
    fun provideApi(retrofit: Retrofit): Api
    {
        return retrofit.create(Api::class.java)
    }
}