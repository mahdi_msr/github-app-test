package ir.mahdimsr.githubapp.provider

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import dagger.hilt.components.SingletonComponent
import ir.mahdimsr.githubapp.repository.FollowerRepository
import ir.mahdimsr.githubapp.repository.LoginRepository
import ir.mahdimsr.githubapp.repository.StarredRepository
import ir.mahdimsr.githubapp.repository.SubscribeRepository
import ir.mahdimsr.githubapp.retrofit.Api
import ir.mahdimsr.githubapp.retrofit.TokenManager
import ir.mahdimsr.githubapp.room.dao.FollowerDao
import ir.mahdimsr.githubapp.room.dao.StarredDao
import ir.mahdimsr.githubapp.room.dao.SubscribeDao
import javax.inject.Singleton

@InstallIn(ActivityRetainedComponent::class)
@Module
object RepositoryProvider
{
    @ActivityRetainedScoped
    @Provides
    fun provideFollowerRepository(followerDao: FollowerDao,api: Api) : FollowerRepository
    {
        return FollowerRepository(followerDao, api)
    }

    @ActivityRetainedScoped
    @Provides
    fun provideSubscribeRepository(subscribeDao: SubscribeDao,api: Api) : SubscribeRepository
    {
        return SubscribeRepository(subscribeDao, api)
    }

    @ActivityRetainedScoped
    @Provides
    fun provideStarredRepository(starredDao: StarredDao,api: Api) : StarredRepository
    {
        return StarredRepository(starredDao, api)
    }

    @ActivityRetainedScoped
    @Provides
    fun provideLoginRepository(followerDao: FollowerDao,starredDao: StarredDao,subscribeDao: SubscribeDao,tokenManager: TokenManager) : LoginRepository
    {
        return LoginRepository(followerDao,starredDao,subscribeDao,tokenManager)
    }
}