package ir.mahdimsr.githubapp.provider

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityRetainedScoped
import dagger.hilt.components.SingletonComponent
import ir.mahdimsr.githubapp.retrofit.TokenManager
import javax.inject.Singleton

@Module
@InstallIn(ActivityRetainedComponent::class)
object TokenManagerProvider
{
    @ActivityRetainedScoped
    @Provides
    fun provideTokenManager(
            @ApplicationContext
            context: Context
                           ): TokenManager
    {
        return TokenManager(context)
    }
}