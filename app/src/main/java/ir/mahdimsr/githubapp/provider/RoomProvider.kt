package ir.mahdimsr.githubapp.provider

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ir.mahdimsr.githubapp.room.AppDB
import ir.mahdimsr.githubapp.room.dao.FollowerDao
import ir.mahdimsr.githubapp.room.dao.StarredDao
import ir.mahdimsr.githubapp.room.dao.SubscribeDao
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomProvider
{

    @Singleton
    @Provides
    fun provideDatabase(
            @ApplicationContext
            context: Context
                       ): AppDB
    {
        return Room.databaseBuilder(
                context,
                AppDB::class.java,
                AppDB.DB_NAME
                                   )
                .fallbackToDestructiveMigration()
                .build()
    }

    @Singleton
    @Provides
    fun provideFollowerDao(appDB: AppDB): FollowerDao
    {
        return appDB.followerDao;
    }

    @Singleton
    @Provides
    fun provideSubscribeDao(appDB: AppDB): SubscribeDao
    {
        return appDB.subscribeDao
    }

    @Singleton
    @Provides
    fun provideStarDao(appDB: AppDB): StarredDao
    {
        return appDB.starredDao;
    }
}