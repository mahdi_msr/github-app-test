package ir.mahdimsr.githubapp.ui.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import dagger.hilt.android.AndroidEntryPoint
import ir.mahdimsr.githubapp.R
import ir.mahdimsr.githubapp.model.Follower
import ir.mahdimsr.githubapp.model.Starred
import ir.mahdimsr.githubapp.ui.view_model.StarredViewModel
import ir.mahdimsr.githubapp.util.DataState

@AndroidEntryPoint
class StarredFragment : Fragment() {

    private val starredViewModel : StarredViewModel by viewModels()

    private lateinit var progressBar: ProgressBar;
    private lateinit var textView: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_starred, container, false)
        findViews(view)

        observeStarredList()
        starredViewModel.getStarredList()


        return view;
    }

    private fun findViews(v: View)
    {
        textView = v.findViewById(R.id.textView)
        progressBar = v.findViewById(R.id.progressBar)
    }

    private fun observeStarredList()
    {
        starredViewModel.starredList.observe(viewLifecycleOwner, Observer { dataState ->

            when(dataState)
            {
                is DataState.Loading ->
                {
                    Log.i("starred" , "Loading")
                    showProgressBar(true)
                }

                is DataState.Success<List<Starred>> ->
                {
                    Log.i("starred" , "api size : ${dataState.data.size}")
                    showProgressBar(false)
                    textView.text = "From Api: \n \n \n"
                    showData(dataState.data)
                }

                is DataState.GetFromCache<List<Starred>> ->
                {
                    Log.i("starred" , "local size : ${dataState.data.size}")
                    showProgressBar(false)
                    textView.text = "From Cache: \n \n \n"
                    showData(dataState.data)
                }

            }

        })
    }

    private fun showProgressBar(isShow: Boolean)
    {
        if (isShow)
        {
            progressBar.visibility = View.VISIBLE
        }
        else
        {
            progressBar.visibility = View.GONE
        }
    }

    private fun showData(starredList: List<Starred>)
    {
        starredList.forEach { starred ->

            textView.text = textView.text.toString() + "\n" + starred.name
        }
    }

}