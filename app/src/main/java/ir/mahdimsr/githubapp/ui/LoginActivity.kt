package ir.mahdimsr.githubapp.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.AndroidEntryPoint
import ir.mahdimsr.githubapp.R
import ir.mahdimsr.githubapp.retrofit.TokenManager
import ir.mahdimsr.githubapp.ui.view_model.LoginViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class LoginActivity : AppCompatActivity()
{
    private val loginViewModel: LoginViewModel by viewModels()

    private lateinit var loginButton: Button
    private lateinit var loginAsButton: Button
    private lateinit var editText: EditText

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        findViews()

        loginButton.setOnClickListener {

            val username = editText.text.toString().trim()

            if (username.isNotEmpty())
            {
                // save as token
                if (loginViewModel.isTokenSaved(username))
                {
                    // clear database
                    loginViewModel.viewModelScope.launch {

                        loginViewModel.clearDatabase()
                    }

                    // navigate to mainActivity
                    navigateToMainActivity()

                }
            }
            else
            {
                Toast.makeText(this, "Username is Empty", Toast.LENGTH_LONG).show()
            }
        }


        // loginAs Button
        if (loginViewModel.hasToken())
        {
            loginAsButton.text = "Login AS ${loginViewModel.getLastToken()}"

        }
        else
        {
            loginAsButton.visibility = View.GONE
        }

    }

    private fun findViews()
    {
        loginButton = findViewById(R.id.loginButton)
        loginAsButton = findViewById(R.id.loginAsButton)
        editText = findViewById(R.id.editText)
    }

    private fun navigateToMainActivity()
    {
        val intent = Intent(this , MainActivity::class.java)

        startActivity(intent)
    }

    private fun  clearDatabase()
    {

    }

}