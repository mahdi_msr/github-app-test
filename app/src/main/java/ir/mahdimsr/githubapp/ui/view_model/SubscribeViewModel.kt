package ir.mahdimsr.githubapp.ui.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.mahdimsr.githubapp.model.Subscribe
import ir.mahdimsr.githubapp.repository.SubscribeRepository
import ir.mahdimsr.githubapp.util.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SubscribeViewModel
@Inject
constructor(private val repository: SubscribeRepository) : ViewModel()
{
    private val _subscribes : MutableLiveData<DataState<List<Subscribe>>> = MutableLiveData();

    val subscribes : LiveData<DataState<List<Subscribe>>> get() = _subscribes

    fun getSubscribe()
    {
        viewModelScope.launch {

            repository.getSubscribes().onEach { dataState ->

                _subscribes.value = dataState

            }.launchIn(viewModelScope)
        }
    }
}