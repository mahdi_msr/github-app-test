package ir.mahdimsr.githubapp.ui.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.AndroidEntryPoint
import ir.mahdimsr.githubapp.R
import ir.mahdimsr.githubapp.model.Follower
import ir.mahdimsr.githubapp.ui.view_model.FollowerViewModel
import ir.mahdimsr.githubapp.util.DataState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

@AndroidEntryPoint
class FollowerFragment : Fragment()
{
    private val followerViewModel: FollowerViewModel by viewModels()

    private lateinit var progressBar: ProgressBar;
    private lateinit var textView: TextView

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
                             ): View?
    {
        val view = inflater.inflate(R.layout.fragment_follower, container, false)
        findViews(view)

        observeFollowers()

        followerViewModel.getFollowers()


        return view;
    }

    private fun findViews(v: View)
    {
        textView = v.findViewById(R.id.textView)
        progressBar = v.findViewById(R.id.progressBar)
    }

    private fun observeFollowers()
    {

        followerViewModel.followers.observe(viewLifecycleOwner, Observer { dataState ->
            when (dataState)
            {
                is DataState.Success<List<Follower>>      ->
                {
                    Log.i("hovita", "success in fragment")
                    showProgressBar(false)
                    textView.text = "From Api: \n \n \n"
                    showData(dataState.data)
                }

                is DataState.Loading                      ->
                {
                    Log.i("hovita", "loading")
                    showProgressBar(true)
                }

                is DataState.GetFromCache<List<Follower>> ->
                {
                    Log.i("hovita", "getFromCache")
                    showProgressBar(false)
                    textView.text = "From Cache: \n \n \n"
                    showData(dataState.data)
                }
            }
        })

    }

    private fun showProgressBar(isShow: Boolean)
    {
        if (isShow)
        {
            progressBar.visibility = View.VISIBLE
        }
        else
        {
            progressBar.visibility = View.GONE
        }
    }

    private fun showData(followers: List<Follower>)
    {
        followers.forEach { follower ->

            textView.text = textView.text.toString() + "\n" + follower.name
        }
    }

}