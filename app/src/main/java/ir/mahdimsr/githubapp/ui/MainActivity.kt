package ir.mahdimsr.githubapp.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint
import ir.mahdimsr.githubapp.R
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity()
{
    private lateinit var bottomNavigationView: BottomNavigationView
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViews()

        bottomNavigationView.setupWithNavController(navController)

        val appBarConfiguration = AppBarConfiguration(setOf(R.id.followerFragment,R.id.starredFragment,R.id.subscribeFragment))
        setupActionBarWithNavController(navController,appBarConfiguration)
    }

    private fun findViews()
    {
        bottomNavigationView = findViewById(R.id.bottomNavigationView)
        navController = findNavController(R.id.fragment)
    }

    override fun onBackPressed()
    {
        startActivity(Intent(this,LoginActivity::class.java));
    }
}