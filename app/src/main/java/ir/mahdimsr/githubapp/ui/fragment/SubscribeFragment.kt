package ir.mahdimsr.githubapp.ui.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.AndroidEntryPoint
import ir.mahdimsr.githubapp.R
import ir.mahdimsr.githubapp.model.Follower
import ir.mahdimsr.githubapp.model.Subscribe
import ir.mahdimsr.githubapp.ui.view_model.SubscribeViewModel
import ir.mahdimsr.githubapp.util.DataState

@AndroidEntryPoint
class SubscribeFragment : Fragment()
{

    private val subscribeViewModel : SubscribeViewModel by viewModels()

    private lateinit var progressBar: ProgressBar;
    private lateinit var textView: TextView

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
                             ): View?
    {
        val view =  inflater.inflate(R.layout.fragment_subscribe, container, false)
        findViews(view)

        observeSubscribes()

        subscribeViewModel.getSubscribe()

        return view;
    }

    private fun findViews(v: View)
    {
        textView = v.findViewById(R.id.textView)
        progressBar = v.findViewById(R.id.progressBar)
    }

    private fun observeSubscribes()
    {
        Log.i("sub" , "here we are")


        subscribeViewModel.subscribes.observe(viewLifecycleOwner, Observer {dataState ->
            when(dataState)
            {
                is DataState.Success<List<Subscribe>> ->
                {
                    Log.i("sub" , "success from api ${dataState.data.size}")
                    showProgressBar(false)
                    textView.text = "From Api: \n \n \n"
                    showData(dataState.data)
                }

                is DataState.GetFromCache<List<Subscribe>> ->
                {
                    Log.i("sub" , "success from cache ${dataState.data.size}")
                    showProgressBar(false)
                    textView.text = "From Cache: \n \n \n"
                    showData(dataState.data)
                }

                is DataState.Loading ->
                {
                    Log.i("sub" , "loading")
                    showProgressBar(true)
                }
            }
        })
    }

    private fun showProgressBar(isShow: Boolean)
    {
        if (isShow)
        {
            progressBar.visibility = View.VISIBLE
        }
        else
        {
            progressBar.visibility = View.GONE
        }
    }

    private fun showData(subscribes: List<Subscribe>)
    {
        subscribes.forEach { subscribe ->

            textView.text = textView.text.toString() + "\n" + subscribe.name
        }
    }

}