package ir.mahdimsr.githubapp.ui.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.mahdimsr.githubapp.model.Starred
import ir.mahdimsr.githubapp.repository.StarredRepository
import ir.mahdimsr.githubapp.util.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class StarredViewModel
@Inject
constructor(private val repository: StarredRepository) : ViewModel()
{
    private val _starredList: MutableLiveData<DataState<List<Starred>>> = MutableLiveData()

    val starredList: LiveData<DataState<List<Starred>>> get() = _starredList

    fun getStarredList()
    {
        viewModelScope.launch {

            repository.getStarred().onEach { dataState ->

                _starredList.value = dataState
            }.launchIn(viewModelScope)
        }
    }
}