package ir.mahdimsr.githubapp.ui.view_model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.mahdimsr.githubapp.repository.LoginRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel
@Inject
constructor(private val repository: LoginRepository) : ViewModel()
{

    suspend fun clearDatabase()
    {

        repository.clearDatabase()
    }

    fun isTokenSaved(token: String): Boolean
    {
        repository.saveOrRefreshToken(token)

        return repository.hasToken()!!
    }

    fun hasToken(): Boolean
    {
        return repository.hasToken()!!
    }

    fun getLastToken(): String
    {
        return repository.getToken()!!
    }

}