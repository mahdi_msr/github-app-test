package ir.mahdimsr.githubapp.ui.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.mahdimsr.githubapp.model.Follower
import ir.mahdimsr.githubapp.repository.FollowerRepository
import ir.mahdimsr.githubapp.util.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FollowerViewModel
@Inject
constructor(private val repository: FollowerRepository) : ViewModel()
{
    private val _followers : MutableLiveData<DataState<List<Follower>>> = MutableLiveData()

    val followers : LiveData<DataState<List<Follower>>> get() = _followers

    fun getFollowers()
    {
        viewModelScope.launch {

            repository.getFollowers().onEach { dataState -> _followers.value = dataState }.launchIn(viewModelScope)
        }
    }
}