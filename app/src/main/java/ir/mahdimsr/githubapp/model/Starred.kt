package ir.mahdimsr.githubapp.model

data class Starred(

    val id: Int,
    val name: String,
    val repositoryName: String
                  )