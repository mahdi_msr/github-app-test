package ir.mahdimsr.githubapp.model

data class Subscribe(
        var id: Int,
        var name: String,
        var description: String
                    )