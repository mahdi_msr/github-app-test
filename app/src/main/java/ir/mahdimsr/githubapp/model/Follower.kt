package ir.mahdimsr.githubapp.model

data class Follower
(
        var id: Int,
        var name: String,
        var image: String
)