package ir.mahdimsr.githubapp.util

import java.lang.Exception

sealed class DataState<out R>
{
    data class Success<out T>(val data: T) : DataState<T>()

    data class GetFromCache<out T>(val data:T) : DataState<T>()

    object Error : DataState<Nothing>()

    object Loading : DataState<Nothing>()
}