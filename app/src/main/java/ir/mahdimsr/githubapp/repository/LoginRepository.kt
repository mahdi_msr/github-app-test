package ir.mahdimsr.githubapp.repository

import android.util.Log
import ir.mahdimsr.githubapp.retrofit.TokenManager
import ir.mahdimsr.githubapp.room.dao.FollowerDao
import ir.mahdimsr.githubapp.room.dao.StarredDao
import ir.mahdimsr.githubapp.room.dao.SubscribeDao
import javax.inject.Inject

class LoginRepository
@Inject
constructor(private val followerDao: FollowerDao, private val starredDao: StarredDao, private val subscribeDao: SubscribeDao,private val tokenManager: TokenManager)
{



    suspend fun clearDatabase()
    {
        followerDao.deleteAll()
        starredDao.deleteAll()
        subscribeDao.deleteAll()

        Log.i("loginRepo" , "database cleared")
    }

    fun saveOrRefreshToken(token : String) : Boolean?
    {
        tokenManager.saveOrRefreshToken(token)

        return tokenManager.isTokenSaved(token)

    }

    fun hasToken() : Boolean?
    {
        return tokenManager.hasToken()
    }

    fun getToken() : String?
    {
        return tokenManager.getToken()
    }


}