package ir.mahdimsr.githubapp.repository

import android.util.Log
import ir.mahdimsr.githubapp.model.Starred
import ir.mahdimsr.githubapp.retrofit.Api
import ir.mahdimsr.githubapp.retrofit.TokenManager
import ir.mahdimsr.githubapp.retrofit.mapper.StarredApiMapper
import ir.mahdimsr.githubapp.room.dao.StarredDao
import ir.mahdimsr.githubapp.room.mapper.StarredRoomMapper
import ir.mahdimsr.githubapp.util.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class StarredRepository
@Inject
constructor(private val starredDao: StarredDao, private val api: Api)
{
    private val starredApiMapper : StarredApiMapper = StarredApiMapper();
    private val starRoomMapper : StarredRoomMapper = StarredRoomMapper();

    suspend fun getStarred() : Flow<DataState<List<Starred>>> = flow {

        try
        {
            emit(DataState.Loading)

            val starredList = getStarredFromApi()

            emit(DataState.Success(starredList))

        }
        catch ( e : Exception)
        {
            Log.e("hovita" , "starred Repository: $e")

            val starredList = getStarredFromLocal()

            emit(DataState.GetFromCache(starredList))
        }

    }

    private suspend fun getStarredFromApi() : List<Starred>
    {
        val starredApiList = api.getStarred()

        // map to model
        val starredList = starredApiMapper.fromEntityList(starredApiList)

        cacheStarred(starredList)

        return starredList
    }

    private suspend fun getStarredFromLocal() : List<Starred>
    {
        val starredRoomList  = starredDao.all()

        val starredList = starRoomMapper.fromEntityList(starredRoomList)

        return starredList
    }

    private suspend fun cacheStarred(starredList : List<Starred>)
    {
        val starredRoomList = starRoomMapper.toEntityList(starredList)

        starredRoomList.forEach {

            starredDao.insert(it)
        }
    }

}