package ir.mahdimsr.githubapp.repository

import android.util.Log
import ir.mahdimsr.githubapp.model.Subscribe
import ir.mahdimsr.githubapp.retrofit.Api
import ir.mahdimsr.githubapp.retrofit.TokenManager
import ir.mahdimsr.githubapp.retrofit.entity.SubscribeApiEntity
import ir.mahdimsr.githubapp.retrofit.mapper.SubscribeApiMapper
import ir.mahdimsr.githubapp.room.dao.SubscribeDao
import ir.mahdimsr.githubapp.room.entity.SubscribeRoomEntity
import ir.mahdimsr.githubapp.room.mapper.SubscribeRoomMapper
import ir.mahdimsr.githubapp.util.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SubscribeRepository
@Inject
constructor(private val subscribeDao: SubscribeDao, private val api: Api)
{
    private val subscribeApiMapper: SubscribeApiMapper = SubscribeApiMapper()
    private val subscribeRoomMapper: SubscribeRoomMapper = SubscribeRoomMapper()

    suspend fun getSubscribes(): Flow<DataState<List<Subscribe>>> = flow {

        try
        {
            emit(DataState.Loading)

            var subscribes: List<Subscribe> = getSubscribesFromApi()

            emit(DataState.Success(subscribes))

        }
        catch (e: Exception)
        {
            emit(DataState.Loading)

            Log.e("sub", "subscribe repository: $e")
            emit(DataState.Error)

            var subscribes: List<Subscribe> = getSubscribesFromLocal();

            emit(DataState.GetFromCache(subscribes))
        }

    }

    private suspend fun getSubscribesFromApi(): List<Subscribe>
    {
        var apiSubscribes: List<SubscribeApiEntity> = api.getSubscribes()

        var subscribes: List<Subscribe> = subscribeApiMapper.fromEntityList(apiSubscribes)

        cacheSubscribeList(subscribes)

        return subscribes;
    }

    private suspend fun getSubscribesFromLocal(): List<Subscribe>
    {
        var roomSubscribes: List<SubscribeRoomEntity> = subscribeDao.all()

        var subscribes: List<Subscribe> = subscribeRoomMapper.fromListEntity(roomSubscribes)

        return subscribes;
    }

    private suspend fun cacheSubscribeList(subscribes: List<Subscribe>)
    {
        var roomSubscribe = subscribeRoomMapper.toListEntity(subscribes)

        roomSubscribe.forEach {
            subscribeDao.insert(it)
        }
    }
}