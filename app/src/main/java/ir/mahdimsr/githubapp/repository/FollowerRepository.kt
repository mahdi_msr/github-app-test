package ir.mahdimsr.githubapp.repository

import android.content.Context
import android.util.Log
import ir.mahdimsr.githubapp.model.Follower
import ir.mahdimsr.githubapp.retrofit.Api
import ir.mahdimsr.githubapp.retrofit.TokenManager
import ir.mahdimsr.githubapp.retrofit.mapper.FollowerApiMapper
import ir.mahdimsr.githubapp.retrofit.entity.FollowerApiEntity
import ir.mahdimsr.githubapp.room.AppDB
import ir.mahdimsr.githubapp.room.mapper.FollowerRoomMapper
import ir.mahdimsr.githubapp.room.dao.FollowerDao
import ir.mahdimsr.githubapp.room.entity.FollowerRoomEntity
import ir.mahdimsr.githubapp.util.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Call
import retrofit2.Callback
import javax.inject.Inject

class FollowerRepository
@Inject
constructor(private val followerDao: FollowerDao, private val api: Api)
{
    private var followerApiMapper: FollowerApiMapper = FollowerApiMapper()
    private var followerRoomMapper: FollowerRoomMapper = FollowerRoomMapper()

    suspend fun getFollowers(): Flow<DataState<List<Follower>>> = flow {

        emit(DataState.Loading)

        try
        {
            val followers : List<Follower> = getFollowersFromApi()

            emit(DataState.Success(followers))
        }
        catch (e: Exception)
        {
            Log.e("hovita",e.toString())

            var followers : List<Follower> = getFollowersFromLocal()

            emit(DataState.GetFromCache(followers))
        }

    }

    private suspend fun getFollowersFromLocal() : List<Follower>
    {
        val localFollowers : List<FollowerRoomEntity> = followerDao.all()
        val followers : List<Follower> = followerRoomMapper.fromEntityList(localFollowers)

        Log.i("follow" , "local size is: ${followers.size}")

        return followers;
    }

    private suspend fun getFollowersFromApi() : List<Follower>
    {
        val apiFollowers : List<FollowerApiEntity> = api.getFollowers()


        Log.i("follow","api size:  ${apiFollowers.size}  ")

        // convert to model
        val followers : List<Follower> = followerApiMapper.fromEntityList(apiFollowers)

        cacheApiFollowers(followers)

        return followers;
    }


    private suspend fun cacheApiFollowers(followers : List<Follower>)
    {
        val roomFollowers : List<FollowerRoomEntity> = followerRoomMapper.toEntityList(followers)

        roomFollowers.forEach {

            followerDao.insert(it)
        }
    }

}