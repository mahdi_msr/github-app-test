package ir.mahdimsr.githubapp.room.dao

import androidx.room.*
import ir.mahdimsr.githubapp.room.entity.SubscribeRoomEntity

@Dao
interface SubscribeDao
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(subscribeRoomEntity: SubscribeRoomEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(subscribeRoomEntity: SubscribeRoomEntity)

    @Delete
    suspend fun delete(subscribeRoomEntity: SubscribeRoomEntity)

    @Query("DELETE FROM subscribes")
    suspend fun deleteAll()

    @Query("SELECT * FROM subscribes WHERE id = :id")
    suspend fun select(id : Int) : SubscribeRoomEntity

    @Query("SELECT * FROM subscribes")
    suspend fun all() : List<SubscribeRoomEntity>
}