package ir.mahdimsr.githubapp.room.dao

import androidx.room.*
import ir.mahdimsr.githubapp.room.entity.FollowerRoomEntity

@Dao
interface FollowerDao
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity: FollowerRoomEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(entity: FollowerRoomEntity)

    @Delete
    suspend fun delete(entity: FollowerRoomEntity)

    @Query("DELETE FROM followers")
    suspend fun deleteAll()

    @Query("SELECT * FROM followers WHERE id = :id")
    suspend fun select(id: Int) : FollowerRoomEntity?

    @Query("SELECT * FROM followers")
    suspend fun all() : List<FollowerRoomEntity>

}