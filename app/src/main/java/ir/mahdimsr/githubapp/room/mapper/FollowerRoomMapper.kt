package ir.mahdimsr.githubapp.room.mapper

import ir.mahdimsr.githubapp.model.Follower
import ir.mahdimsr.githubapp.room.entity.FollowerRoomEntity
import ir.mahdimsr.githubapp.util.EntityMapper
import javax.inject.Inject

class FollowerRoomMapper
@Inject
constructor() : EntityMapper<FollowerRoomEntity, Follower>
{
    override fun fromEntity(entity: FollowerRoomEntity): Follower
    {

        return Follower(
                id = entity.id,
                name = entity.name,
                image = entity.avatarUrl
                       )
    }

    override fun toEntity(model: Follower): FollowerRoomEntity
    {
        return FollowerRoomEntity(
                id = model.id,
                name = model.name,
                avatarUrl = model.image
                                                                                      )
    }

    fun toEntityList(models : List<Follower>) : List<FollowerRoomEntity>
    {
        return models.map { toEntity(it) }
    }

    fun fromEntityList(entities : List<FollowerRoomEntity>) : List<Follower>
    {
        return entities.map { fromEntity(it) }
    }
}