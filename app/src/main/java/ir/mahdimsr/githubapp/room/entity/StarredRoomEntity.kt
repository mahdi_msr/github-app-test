package ir.mahdimsr.githubapp.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "starred")
data class StarredRoomEntity(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    val id : Int,

    @ColumnInfo(name ="name")
    var name : String,

    @ColumnInfo(name = "repository_name")
    var repositoryName : String

                            )