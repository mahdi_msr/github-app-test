package ir.mahdimsr.githubapp.room.mapper

import ir.mahdimsr.githubapp.model.Subscribe
import ir.mahdimsr.githubapp.room.entity.SubscribeRoomEntity
import ir.mahdimsr.githubapp.util.EntityMapper
import javax.inject.Inject

class SubscribeRoomMapper
@Inject
constructor() : EntityMapper<SubscribeRoomEntity,Subscribe>
{
    override fun fromEntity(entity: SubscribeRoomEntity): Subscribe
    {
        return Subscribe(
                id = entity.id,
                name = entity.name,
                description = entity.description
                        )
    }

    override fun toEntity(model: Subscribe): SubscribeRoomEntity
    {
        return SubscribeRoomEntity(
                id = model.id,
                name = model.name,
                description = model.description
                                  )
    }


    fun fromListEntity(entities: List<SubscribeRoomEntity>) : List<Subscribe>
    {
        return entities.map { fromEntity(it) }
    }


    fun toListEntity(entities: List<Subscribe>) : List<SubscribeRoomEntity>
    {
        return entities.map { toEntity(it) }
    }

}