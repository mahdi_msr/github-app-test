package ir.mahdimsr.githubapp.room.dao

import androidx.room.*
import ir.mahdimsr.githubapp.room.entity.StarredRoomEntity

@Dao
interface StarredDao
{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(starRoomEntity: StarredRoomEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(starRoomEntity: StarredRoomEntity)

    @Delete
    suspend fun delete(starRoomEntity: StarredRoomEntity)

    @Query("DELETE FROM starred")
    suspend fun deleteAll()

    @Query("SELECT * FROM starred WHERE id = :id")
    suspend fun select(id: Int): StarredRoomEntity?

    @Query("SELECT * FROM starred")
    suspend fun all(): List<StarredRoomEntity>
}