package ir.mahdimsr.githubapp.room.mapper

import ir.mahdimsr.githubapp.model.Starred
import ir.mahdimsr.githubapp.room.entity.StarredRoomEntity
import ir.mahdimsr.githubapp.util.EntityMapper

class StarredRoomMapper : EntityMapper<StarredRoomEntity, Starred>
{
    override fun fromEntity(entity: StarredRoomEntity): Starred
    {
        return Starred(
                id = entity.id,
                name = entity.name,
                repositoryName = entity.repositoryName
                      )
    }

    override fun toEntity(model: Starred): StarredRoomEntity
    {
        return StarredRoomEntity(
                id = model.id,
                name = model.name,
                repositoryName = model.repositoryName
                                )
    }

    fun toEntityList(models : List<Starred>) : List<StarredRoomEntity>
    {
        return models.map { toEntity(it) }
    }

    fun fromEntityList(entities : List<StarredRoomEntity>) : List<Starred>
    {
        return  entities.map { fromEntity(it) }
    }
}