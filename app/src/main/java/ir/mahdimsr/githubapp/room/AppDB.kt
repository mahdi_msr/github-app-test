package ir.mahdimsr.githubapp.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ir.mahdimsr.githubapp.room.dao.FollowerDao
import ir.mahdimsr.githubapp.room.dao.StarredDao
import ir.mahdimsr.githubapp.room.dao.SubscribeDao
import ir.mahdimsr.githubapp.room.entity.FollowerRoomEntity
import ir.mahdimsr.githubapp.room.entity.StarredRoomEntity
import ir.mahdimsr.githubapp.room.entity.SubscribeRoomEntity

@Database(
        entities = [FollowerRoomEntity::class, SubscribeRoomEntity::class, StarredRoomEntity::class],
        version = 2
         )
abstract class AppDB : RoomDatabase()
{
    abstract val followerDao: FollowerDao
    abstract val subscribeDao: SubscribeDao
    abstract val starredDao: StarredDao

    companion object
    {
        const val DB_NAME = "githubApp_db";

        @Volatile
        private var INSTANCE: AppDB? = null
        fun getInstance(context: Context): AppDB
        {
            synchronized(this)
            {
                var instance = INSTANCE
                if (instance == null)
                {
                    instance =
                            Room.databaseBuilder(context.applicationContext, AppDB::class.java, DB_NAME)
                                    .build()
                }

                return instance
            }

        }
    }
}