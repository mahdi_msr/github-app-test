package ir.mahdimsr.githubapp.retrofit.entity

data class SubscribeApiEntity
(
        var id: Int,
        var name: String,
        var description: String
)