package ir.mahdimsr.githubapp.retrofit.entity

data class StarredApiEntity(
        val id: Int,
        val name: String,
        val full_name: String
                           )