package ir.mahdimsr.githubapp.retrofit

import ir.mahdimsr.githubapp.retrofit.entity.FollowerApiEntity
import ir.mahdimsr.githubapp.retrofit.entity.StarredApiEntity
import ir.mahdimsr.githubapp.retrofit.entity.SubscribeApiEntity
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface Api
{
    @GET("followers")
    suspend fun getFollowers() : List<FollowerApiEntity>


    @GET("subscriptions")
    suspend fun getSubscribes() : List<SubscribeApiEntity>

    @GET("starred")
    suspend fun getStarred() : List<StarredApiEntity>
}