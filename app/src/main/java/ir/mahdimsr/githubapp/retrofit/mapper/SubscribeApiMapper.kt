package ir.mahdimsr.githubapp.retrofit.mapper

import ir.mahdimsr.githubapp.model.Subscribe
import ir.mahdimsr.githubapp.retrofit.entity.SubscribeApiEntity
import ir.mahdimsr.githubapp.util.EntityMapper
import javax.inject.Inject

class SubscribeApiMapper
@Inject
constructor() : EntityMapper<SubscribeApiEntity, Subscribe>
{
    override fun fromEntity(entity: SubscribeApiEntity): Subscribe
    {
        return Subscribe(
                id = entity.id,
                name = entity.name,
                description = entity.description
                        )
    }

    override fun toEntity(model: Subscribe): SubscribeApiEntity
    {
        return SubscribeApiEntity(
                id = model.id,
                name = model.name,
                description = model.description
                                 )
    }

    fun fromEntityList(entities : List<SubscribeApiEntity>) : List<Subscribe>
    {
        return  entities.map { fromEntity(it) }
    }

}