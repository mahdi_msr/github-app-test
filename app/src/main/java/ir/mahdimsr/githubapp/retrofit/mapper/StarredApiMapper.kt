package ir.mahdimsr.githubapp.retrofit.mapper

import ir.mahdimsr.githubapp.model.Starred
import ir.mahdimsr.githubapp.retrofit.entity.StarredApiEntity
import ir.mahdimsr.githubapp.util.EntityMapper

class StarredApiMapper : EntityMapper<StarredApiEntity,Starred>
{
    override fun fromEntity(entity: StarredApiEntity): Starred
    {
        return Starred(
                id = entity.id,
                name = entity.name,
                repositoryName = entity.full_name
                      )
    }

    override fun toEntity(model: Starred): StarredApiEntity
    {
        return StarredApiEntity(
                id = model.id,
                name = model.name,
                full_name = model.repositoryName
                               )
    }

    fun fromEntityList(entities : List<StarredApiEntity>) : List<Starred>
    {
        return entities.map { fromEntity(it) }
    }
}