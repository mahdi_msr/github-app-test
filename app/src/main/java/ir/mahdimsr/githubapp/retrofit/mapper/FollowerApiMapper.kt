package ir.mahdimsr.githubapp.retrofit.mapper

import ir.mahdimsr.githubapp.model.Follower
import ir.mahdimsr.githubapp.retrofit.entity.FollowerApiEntity
import ir.mahdimsr.githubapp.util.EntityMapper
import javax.inject.Inject

class FollowerApiMapper
@Inject
constructor() : EntityMapper<FollowerApiEntity, Follower>
{

    // follower mapper
    override fun fromEntity(entity: FollowerApiEntity): Follower
    {
        return Follower(
                id = entity.id,
                name = entity.login,
                image = entity.avatar_url
                       )
    }

    override fun toEntity(model: Follower): FollowerApiEntity
    {
        return FollowerApiEntity(
                id = model.id,
                login = model.name,
                avatar_url = model.image
                                )
    }

    fun fromEntityList(entities: List<FollowerApiEntity>) : List<Follower>
    {
        return entities.map { fromEntity(it) }
    }
}