package ir.mahdimsr.githubapp.retrofit.entity

data class FollowerApiEntity
(
        var id: Int,
        var login: String,
        var avatar_url: String
)