package ir.mahdimsr.githubapp.retrofit

import android.content.Context
import android.content.SharedPreferences
import android.util.Base64
import javax.crypto.Cipher
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec
import javax.inject.Inject

class TokenManager
//@Inject
constructor(private val context: Context)
{

    private val _ALIAS_NAME = "PRIVATE_TOKEN"

    private val secretKey = "tK5UTui+DPh8lIlBxya5XVsmeDCoUl6vHhdIESMB6sQ="
    private val salt = "QWlGNHNhMTJTQWZ2bGhpV3U="
    private val iv = "bVQzNFNhRkQ1Njc4UUFaWA=="


    private fun encrypt(data: String): String?
    {
        try
        {
            val ivParameterSpec = IvParameterSpec(Base64.decode(iv, Base64.DEFAULT))

            val factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
            val spec = PBEKeySpec(secretKey.toCharArray(), Base64.decode(salt, Base64.DEFAULT), 10000, 256)
            val tmp = factory.generateSecret(spec)
            val secretKey = SecretKeySpec(tmp.encoded, "AES")

            val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec)
            return Base64.encodeToString(cipher.doFinal(data.toByteArray(Charsets.UTF_8)), Base64.DEFAULT)
        }
        catch (e: Exception)
        {
            println("Error while encrypting: $e")
        }

        return null
    }

    private fun decrypt(data: String): String?
    {
        try
        {

            val ivParameterSpec = IvParameterSpec(Base64.decode(iv, Base64.DEFAULT))

            val factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
            val spec = PBEKeySpec(secretKey.toCharArray(), Base64.decode(salt, Base64.DEFAULT), 10000, 256)
            val tmp = factory.generateSecret(spec);
            val secretKey = SecretKeySpec(tmp.encoded, "AES")

            val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);
            return String(cipher.doFinal(Base64.decode(data, Base64.DEFAULT)))
        }
        catch (e: Exception)
        {
            println("Error while decrypting: $e");
        }
        return null
    }

    fun saveOrRefreshToken(token: String)
    {
        val sharedPreferences: SharedPreferences = context.getSharedPreferences(_ALIAS_NAME, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPreferences.edit()

        editor.putString("token", encrypt(token))

        editor.apply()

    }

    fun isTokenSaved(token: String) : Boolean?
    {
        val savedToken = getToken()

        return savedToken?.let { token.contains(it) }
    }

    fun getToken(): String
    {
        val sharedPreferences: SharedPreferences = context.getSharedPreferences(_ALIAS_NAME, Context.MODE_PRIVATE)

        val token = sharedPreferences.getString("token", null)?.let { decrypt(it) }

        return token!!
    }

    fun hasToken() : Boolean
    {
        return getToken() != null
    }


}